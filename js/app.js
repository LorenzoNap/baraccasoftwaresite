(function() {
    var app = angular.module('baraccaController', ['pascalprecht.translate','ui.bootstrap']).config(function($translateProvider) {
        $translateProvider.translations('en', {
            NAVBAR:{
                HOME : "Home",
                PROJECT: "Projects",
                ABOUT_US: "About us",
                TEAM: "Team",
                SERVICES: "Services",
                CONTACT: "Contacts"

            },
            CREATION: "Creations",
            DESCRIPTION_ONE:"We are not only codes. We move with the rhythm of our ideas and our passions. Enter in the world of baraccasoftware!",
            PROJECTS_DESCRIPTION: "Our creations",
            SHOW_ALL_PROJETCS:"All",
            TEAM_ADESION:"Do you want join our team?",
            TEAM_WAIT: "We are waiting for you.",
            ABOUT_US_DESCRIPTION:{
                DESCRIPTION:"A passionate team with innovative ideas.",
                WHO_WE_ARE:"Who we are",
                WHO_WE_ARE_DESCRIPTION: "The project of baraccasoftware was born among the classrooms of university. We are asked ourselves :'Why we don't use our ideas and our capacity in order to create something new?'" +
                    ". In this way we chose to build up the baracca to manage our and your ideas.",
                GOALS:"Goals",
                GOALS_DESCRIPTION:"The 'barrack' houses the creations of our team. Furthermore we can help you to realize your project: an android application, a website, a business software and so more!",
                TEAM: "The Team",
                TEAM_DESCRIPTION:"The team is the strong piece of the barrack!. Each member has been chosen by his creativity and capacity to use different technologies."
            },
            TEAM:{
                NAME: "The Team",
                DESCRIPTION:"Little, but strong.",
                ANGELO_NAME:"Angelo 'Angelino' Moroni",
                ANGELO_POSITION:"Developer",
                ANGELO_DESCRIPTION:"Well, Angelo ,in addition of his beauty and sympathy, is one of the developer of Baracca Software."+
                    "He has confusing talents that he doesn't know. He is able to find the right idea at the right time. Currently he work at Pharma Wizard Startup and studies computer science at University of Rome 'Tor Vergata'",
                LORENZO_NAME:"Lorenzo 'nappa' Napoleoni",
                LORENZO_POSITION:"Developer",
                LORENZO_DESCRIPTION:"When he is not busy to rail against not worked programs or to install a new operative system, he usually contributes to carry forward  the barrack"+
                    "He worked at Capgeminì company and now he  studies computer science at University of Rome 'Tor Vergata'",
                EXTERNAL_CONTRIBUTOR:"External Contributor",
                EXTERNAL_CONTRIBUTOR_DESCRIPTION:"Our helpers",
                MATTEO_NAME:"Matteo Proietti",
                MATTEO_POSITION:"App graphic",
                MATTEO_DESCRIPTION:"Matteo is a man with few words. When he speaks, his phrases are sentences or jokes."+ "This is the characteristic of who has clear ideas and who does everything  necessary to realize own projects.",
                DANIELE_PASQUINI_NAME:"Daniele Pasquini",
                DANIELE_POSITION:"App graphic, Tester, Teaser Advertising",
                DANIELE_DESCRIPTION:"Daniele is quiet and shrewd. He prefers to point on facts and quality. He has a mighty flair in order to unveil the smallest errors. He doesn't permit any error",
                OUR_FRIENDS: "Ours friends",
                OUR_FRIENDS_DESCRIPTION: "Strong, togheter",
                EHTRONSOFT_NAME: "Ethronsoft",
                EHTRONSOFT_POSITION: "Ethronsoft, indipendent software house.",
                EHTRONSOFT_DESCRIPTION: "Another little software house but with great potential.",
                D_STUDIO_POSITION: "Graphic Studio",
                D_STUDIO_DESCRIPTION: "Graphic Studio"
            },
            SERVICE_DESCRIPTION:"We can help you to realize your project",
            SERVICE_DESIGN:"Do you need a website?Do you want to present your product? Contact us! We can create your customized website",
            SERVICE_ESHOP:"Do you want sell your products? We can create an ecommerce website with a bunch of functionalities",
            SERVICE_SOCIAL:"Do you think an android app? We can create it for you!",
            SERVICE_STATS:"Do you need an app in order to manage your business? You are in the right place!We realize management software for all taste!",

            QUOTE:"Simplicity is prerequisite for reliability.",
            CONTATCT_DESCRIPTION:"Stay in contact with us!",
            PROJECTS:{
                DSTUDIO_DESCRIPTION: "Born of love for communication, in its most wide-ranging facets, 4D Studio is the reference point for companies and professionals who need to spread on the market. We are dealing with Marketing, Communication and Advertising: three basic aspects to promote your business and expand your business.",
                FIND_THE_HOLE_TITLE: "Find The Hole",
                ROMAN_NUMBERS_TITLE: "Roman Numbers Converter",
                AAA_COLORS_TITLE:"AAA Colors",
                AAA_COLORS_DESCRIPTION:"AAA Colors, challenge your friend to match a lot of colors. One Color, three choices, select the perfect one.Invite your friend and challenge them to be the best.",
                FIND_THE_HOLE_DESCRIPTION:"Find free classrooms of the Faculty of Sciences of the University of Rome Tor Vergata. How many times have you found yourself wandering the halls of the university in search of a free classroom to study? From today, all your problems are solved."+
                " Stop wasting time to ask if a classroom is free. Find The Hole, with a simple tap of the finger, allows you to get a list of the classrooms in which there is no lesson. In addition, you can view the schedule of classes that are held in a specific classroom; report whether vacant or occupied classroom and review the reporting of other users.",
                ROMAN_NUMBERS_DESCRIPTION: "How many times have you dealt with the problems of decipher a roman number? Now there is the app that do this for you! With a comfortable and friendly interface, it will possible to convert a roman number in a arabic number (1,2,3,4,..). Insert the roman number and in automatic way, it will be converted in the corresponding arabic number! If you want know the conversion of an arabic number into a roman number, you must do the opposite: it is very easy!",
                SECURE_NOTES_DESCRIPTION:"Having a smartphone means sometimes having to write our passwords or account credentials in our block note applications. We are also guilty of recording our ATM pins in this unsafe wat: anyone can have access to this information. The solution to his problem is SecureNotes!SecureNote is a crypted block note with a password chosen by the user. In this way only the actual owner of the smartphone can write and read any information (password, account credentials, private information..). This information is saved in a crypted way and  only with your password will you be able to read your information (the password  is chosen during the application inizialization)."+
                    "SecureNotes is not only a password manager, it is something more simple! With a clear user interface it is possible to take a photo (that will be crypted) and add it to our note. An example: when we want to save a friend WI_FI which is often too long, it is easier to take a photo!",
                SWIPE_SMS_DESCRIPTION:"A new application to send sms through swipes! Join the developer community!",
                EASY_FACEBOOK_DESCRIPTION:"Easy Facebook is a simple alternative to official facebook client."+ "This app allows to use your social network as a page in the browser, but you must not open any browser neither type any address."+ "In that way, Easy Facebook is very light."
            }


        }).translations('it', {
            NAVBAR:{
                HOME : "Home",
                PROJECT: "Progetti",
                ABOUT_US: "Su di noi",
                TEAM: "Team",
                SERVICES: "Servizi",
                CONTACT: "Contatti"

            },
            CREATION: "Creazioni",
            DESCRIPTION_ONE:"Noi non siamo solo codice. Noi ci muoviamo al ritmo delle idee e delle nostre passioni. Entra nel mondo di baraccasoftware!",
            PROJECTS_DESCRIPTION: "Le nostre creazioni",
            SHOW_ALL_PROJETCS:"tutte",
            TEAM_ADESION:"Vuoi fare parte del nostro team?",
            TEAM_WAIT: "Ti aspettiamo.",
            ABOUT_US_DESCRIPTION:{
                DESCRIPTION:"Un team appossionato & con idee innovative.",
                WHO_WE_ARE:"Chi siamo",
                WHO_WE_ARE_DESCRIPTION: "Il progetto di baraccasoftware nasce tra le aule di un'università. Ci siamo chiesti 'perchè non usare le nostre idee e le nostre " +
                    "competenze per creare qualcosa di nuovo?'. In questo modo abbiamo deciso di mettere in piedi la baracca per supportare le nostre e le vostre idee.",
                GOALS:"Obiettivi",
                GOALS_DESCRIPTION:"La baracca serve per ospitare le creazioni del proprio team. Inoltre possiamo aiutarvi a realizzare il vostro progetto: un applicazione android,"+
                        "un sito internet, un software gestionale. Basta chiedere!",
                TEAM: "The Team",
                TEAM_DESCRIPTION:"Il team è il pezzo forte della baracca. Ogni individuo è stato scelto per la sua creatività e competenza per l'uso delle più diverse"+
                +"tecnologie."
            },
            TEAM:{
                NAME: "The Team",
                DESCRIPTION:"Piccolo, ma forte.",
                ANGELO_NAME:"Angelo 'Angelino' Moroni",
                ANGELO_POSITION:"Developer",
                ANGELO_DESCRIPTION:"Beh, oltre ad essere bello e simpatico, Angelo e' uno degli sviluppatori software di baraccaSoftware."+
                "Dotato di doti confusionarie che nemmeno lui conosce e' in grado di trovare l'idea giusta al momento giusto. Attualmente lavora presso la"+
                "startup PharmaWizard e frequenta il corso di laurea magistrale in informatica presso l'università di Roma Tor Vergata.",
                LORENZO_NAME:"Lorenzo 'nappa' Napoleoni",
                LORENZO_POSITION:"Developer",
                LORENZO_DESCRIPTION:"Quando non e' occupato a inveire contro i programmi che non funzionano o ad installare qualche nuovo sistema operativo, di solito, contribuisce a portare avanti la baracca."+
                "Ha lavorato preso la società Capgeminì ed ora frequenta il corso di laurea magistrale in informatica presso l'università di Roma Tor Vergata.",
                EXTERNAL_CONTRIBUTOR:"External Contributor",
                EXTERNAL_CONTRIBUTOR_DESCRIPTION:"I nostri aiutanti",
                MATTEO_NAME:"Matteo Proietti",
                MATTEO_POSITION:"Grafica app",
                MATTEO_DESCRIPTION:"Matteo non e' una persona di molte parole, ma quando parla, le sue sono sentenze o prese in giro."+
                "Questa e' una caratteristica di chi ha le idee chiare e fa di tutto affinche' possa realizzare i propri progetti.",
                DANIELE_PASQUINI_NAME:"Daniele Pasquini",
                DANIELE_POSITION:"App graphic, Tester, Teaser Advertising",
                DANIELE_DESCRIPTION:"Silezioso e accorto, Daniele preferisce puntare sui fatti e sulla qualità. Ha un fiuto eccelso nel trovare i piu piccoli errori: con lui non puoi sbagliare.",

                OUR_FRIENDS: "I nostri amici",
                OUR_FRIENDS_DESCRIPTION: "L'unione fa la forza",
                EHTRONSOFT_NAME: "Ethronsoft",
                EHTRONSOFT_POSITION: "Ethronsoft, indipendent software house.",
                EHTRONSOFT_DESCRIPTION: "Un'altra piccola software house pronta a sbalordirvi con applicazioni geniali! ",
                D_STUDIO_POSITION: "Studio Grafico",
                D_STUDIO_DESCRIPTION: "Graphic Studio"



            },
            SERVICE_DESCRIPTION:"Possiamo aiutarti a creare il tuo progetto",
            SERVICE_DESIGN:"Hai bisogno di un sito internet? vuoi presentare un tuo prodotto o semplicemente farti conoscere? Contattatici poissiamo creare il tuo sito su misura",
            SERVICE_ESHOP:"Vuoi vendere i tuoi prodotti? Noi possiamo realizzare uno shop online con tutte le funzionalita' dei più moderni siti di e-commerce.",
            SERVICE_SOCIAL:"Hai in mente un app android? Noi possiamo realizzarla per te.",
            SERVICE_STATS:"Ti serve un applicazione per gestire il tuo buiness? Sei nel posto giusto! Realizziamo software gestionali per tutti i gusti!",

            QUOTE:"La semplicità è un prerequisito per l'affidabilità.",
            CONTATCT_DESCRIPTION:"Rimani in contatto con noi!",
            PROJECTS:{
                DSTUDIO_DESCRIPTION: "Nato dall’amore per la comunicazione, nelle sue piu' ampie sfaccettature, 4D Studio rappresenta il punto di riferimento per aziende e professionisti che hanno necessita' di diffondersi sul mercato. Ci occupiamo di Marketing, Comunicazione e Pubblicita’: tre aspetti fondamentali per promuovere la Vostra attivita' ed ampliare il Vostro business. ",
                MARTA_DESCRIPTION:"COOMING SOON!",
                FIND_THE_HOLE_TITLE: "Find The Hole",
                FIND_THE_HOLE_DESCRIPTION:"Trova le aule libere della facoltà di Scienze dell'università di Roma Tor Vergata. Quante volte ti sei ritrovato a vagare per i corridoi "+
                "dell'università in cerca di un'aula libera per potere studiare? Da oggi tutti i tuoi problemi sono risolti.Basta fare figuracce nell'aprire le porte e subire lo sguardo inferocito del professore che sta tenendo lezione. Basta perdere tempo a chiedere se un aula è libera. Find The Hole, con un semplice tap del dito, ti permette di ottenere la lista delle aule in cui non c'è lezione. Inoltre, è possibile visionare gli orari delle lezioni che si tengono in una specifica aula; segnalare se un'aula libera o occupata e visionare le segnalazione degli altri utenti.",
                AAA_COLORS_TITLE:"AAA Colors",
                AAA_COLORS_DESCRIPTION:"AAA Colors, challenge your friend to match a lot of colors. One Color, three choices, select the perfect one.Invite your friend and challenge them to be the best.",
                ROMAN_NUMBERS_TITLE: "Convertitore Numeri Romani",
                ROMAN_NUMBERS_DESCRIPTION: "Quante volte vi siete trovati a dovere decifrare un numero romano? Ora c'è l'app che lo fai per voi. Con una interfaccia comoda ed intuitiva convertire un numero romano in un numero arabo (1,2,3,4..) sarà semplicissimo. Insersci il numero romano nell'apposito campo e automaticamente sarà convertito nel corrispondente numero arabo."+
                    "Se vuuoi sapere a quale numero romano corrisponde un determinato numero arabo ti basterà fare il contrario: è semplicissimo!",
                SECURE_NOTES_DESCRIPTION:"Non riesci a ricordati tutte le tue password? Secure notes lo fa' per te. Salva in modo del tutto sicuro le tue note,"+
                    "in modo tale che solamente tu, attraverso una password, potrai accedere alle tue informazioni sensibili in modo protetto.",
                SWIPE_SMS_DESCRIPTION:"Una nuova applicazione per mandare SMS con un semplice swipe! Entra nella comunity degli sviluppatori!",
                EASY_FACEBOOK_DESCRIPTION:"EasyFacebook è una semplice alternativa al client ufficiale."+
                "Questa app ti permette di utilizzare il tuo social network preferito come se fosse in un browser: la differenza è che ti toglie la fatica di aprirne uno e digitare l'indirizzo."+
                "In questo modo, EasyFacebook è molto leggera."

            }





        });
        ;
        $translateProvider.preferredLanguage('it');
        //$translateProvider.determinePreferredLanguage();


    });



    var ModalDemoCtrl = function ($scope, $modal, $log) {

        /* Note: The hard coded user object has been commented out, as
         it is now passed as an object parameter from the HTML template.
         */
        /* $scope.user = {
         user: 'name',
         password: null
         };*/

        $scope.open = function (user) {
            $scope.user = user;
            $modal.open({
                templateUrl: user,
                backdrop: true,
                windowClass: 'modal',
                controller: function ($scope, $modalInstance, $log, user) {
                    $scope.user = user;
                    $scope.submit = function () {
                        $log.log('Submiting user info.');
                        $log.log(JSON.stringify(user));
                        $modalInstance.dismiss('cancel');
                    }
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                },
                resolve: {
                    user: function () {
                        return $scope.user;
                    }
                }
            });
        };
    };
    app.controller("ModalDemoCtrl",ModalDemoCtrl);

    app.controller('TranslateController', function($translate, $scope) {


        var userLang = navigator.language || navigator.userLanguage;
        //alert ("The language is: " + userLang);
        if(userLang == "it"){
            $translate.use('it');
        }
        else{
            $translate.use('en');
        }


        $scope.changeLanguage = function (langKey) {
            $translate.use(langKey);
        };
    });



})();



